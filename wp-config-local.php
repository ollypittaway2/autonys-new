<?php

$project = 'autonys';

define("DB_NAME", "autonys");
define("DB_USER", "root");
define("DB_PASSWORD", "root");
define("DB_HOST", "localhost");
//define("WP_DEBUG", true);
//define("WP_DEBUG_DISPLAY", true);
//define("WP_DEBUG_LOG", false);
define("WP_CONTENT_DIR", dirname(__FILE__). "/app" );
define("WP_CONTENT_URL", "http://". $_SERVER["HTTP_HOST"]. "/" . $project . "/app");
define("WPLANG", "en_GB");
