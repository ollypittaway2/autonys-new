jQuery(document).ready(function($) {
	jQuery(".main-holder input[type='submit'], .main-holder input[type='reset']").wrap('<span class="btn-wrapper"></span>');

	jQuery('.post__holder .post-title a, .widget-sidebar .widget-title, .related-posts_h, .comments-h, #respond h3, .title-header, .page-header h1').each(function(index) {
	    //get the first word
	    var firstWord = jQuery(this).text().split(' ')[0];

	    //wrap it with span
	    var replaceWord = "<strong>" + firstWord + "</strong>";

	    //create new string with span included
	    var newString = jQuery(this).html().replace(firstWord, replaceWord);

	    //apply to the divs
	    jQuery(this).html(newString);
    });
});