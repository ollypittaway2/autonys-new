<?php
	// Loads child theme textdomain
	load_child_theme_textdomain( CURRENT_THEME, CHILD_DIR . '/languages' );

	// Loads custom scripts.
	// require_once( 'custom-js.php' );

	add_filter( 'cherry_stickmenu_selector', 'cherry_change_selector' );
	function cherry_change_selector($selector) {
		$selector = '.nav-wrapper';
		return $selector;
	}

	add_action( 'wp_enqueue_scripts', 'cherry_child_custom_scripts' );
	function cherry_child_custom_scripts() {
		wp_enqueue_script( 'my_script', CHILD_URL . '/js/my_script.js', array( 'jquery' ), '1.0' );
	}

	// Add odd/even on sidebar widget
	add_filter('dynamic_sidebar_params', 'my_widget_class');
	function my_widget_class($params) {
		global $widget_num;

		// Widget class
		$class = array();

		// Iterated class
		$widget_num++;

		// Alt class
		if ($widget_num % 2) :
			$class[] = 'widget-even';
		else :
			$class[] = 'widget-odd';
		endif;

		// Join the classes in the array
		$class = join(' ', $class);

		// Interpolate the 'my_widget_class' placeholder
		$params[0]['before_widget'] = str_replace('my_widget_class', $class, $params[0]['before_widget']);
		return $params;
	}

	// Title Box
	if ( !function_exists( 'title_shortcode' ) ) {
		function title_shortcode( $atts, $content = null, $shortcodename = '' ) {
			extract( shortcode_atts(
				array(
					'title'        => '',
					'subtitle'     => '',
					'icon'         => '',
					'custom_class' => '',
				), $atts ) );

			$output = '<div class="title-box clearfix ' . $custom_class . '">';

			// Icon.
			if ( $icon != '' ) {

				// Gets the icon extension.
				$icon_ext = pathinfo( $icon, PATHINFO_EXTENSION );

				if ( empty( $icon_ext ) ) {
					$icon .= '.png';
				}

				$icon_url = CHERRY_PLUGIN_URL . 'includes/images/' . strtolower( $icon );

				if ( defined( 'CHILD_DIR' ) ) {

					if ( file_exists( CHILD_DIR . '/images/' . strtolower( $icon ) ) ) {

						$icon_url = CHILD_URL . '/images/' . strtolower( $icon );

					}

				}

				$output .= '<span class="title-box_icon">';
					$output .= '<img src="' . $icon_url . '" alt="" />';
				$output .= '</span>';

			}

			// Title.
			$output .= '<h2 class="title-box_primary">';
				$output .= $title;
			$output .= '</h2>';

			// Subtitle.
			if ( $subtitle != '' ) {
				$output .= '<h3 class="title-box_secondary">';
					$output .= $subtitle;
				$output .= '</h3>';
			}

			$output .= '</div><!-- //.title-box -->';
			$output = apply_filters( 'cherry_plugin_shortcode_output', $output, $atts, $shortcodename );

			return $output;
		}
		add_shortcode( 'title_box', 'title_shortcode' );
	}

	/**
	 * Service Box
	 *
	 */
	if (!function_exists('service_box_shortcode')) {

		function service_box_shortcode( $atts, $content = null, $shortcodename = '' ) {
			extract(shortcode_atts(
				array(
					'title'        => '',
					'subtitle'     => '',
					'icon'         => '',
					'text'         => '',
					'btn_text'     => __('Read more', CURRENT_THEME),
					'btn_link'     => '',
					'btn_size'     => '',
					'target'       => '',
					'custom_class' => ''
			), $atts));

			$output =  '<div class="service-box '.$custom_class.'">';

			if($icon != 'no'){
				switch ($icon) {
					case 'icon1':
						$output .= '<figure class="icon"><i class="icon-time"></i></figure>';
						break;
					
					default:
						$icon_url = CHERRY_PLUGIN_URL . 'includes/images/' . strtolower($icon) . '.png' ;
						if( defined ('CHILD_DIR') ) {
							if(file_exists(CHILD_DIR.'/images/'.strtolower($icon).'.png')){
								$icon_url = CHILD_URL.'/images/'.strtolower($icon).'.png';
							}
						}
						$output .= '<figure class="icon"><img src="'.$icon_url.'" alt="" /></figure>';
						break;
				}
			}

			$output .= '<div class="service-box_body">';

			if ($title!="") {
				$output .= '<h2 class="title">';
				$output .= $title;
				$output .= '</h2>';
			}
			if ($subtitle!="") {
				$output .= '<h5 class="sub-title">';
				$output .= $subtitle;
				$output .= '</h5>';
			}
			if ($text!="") {
				$output .= '<div class="service-box_txt">';
				$output .= $text;
				$output .= '</div>';
			}
			if ($btn_link!="") {
				$output .=  '<div class="btn-align"><a href="'.$btn_link.'" title="'.$btn_text.'" class="btn btn-inverse btn-'.$btn_size.' btn-primary " target="'.$target.'">';
				$output .= $btn_text;
				$output .= '</a></div>';
			}
			$output .= '</div>';
			$output .= '</div><!-- /Service Box -->';

			$output = apply_filters( 'cherry_plugin_shortcode_output', $output, $atts, $shortcodename );

			return $output;
		}
		add_shortcode('service_box', 'service_box_shortcode');

	}

	// google_api_map
	if ( !function_exists('google_map_api_shortcode') ) {
		function google_map_api_shortcode( $atts, $content = null ) {
			extract(shortcode_atts(array(
					'lat_value'      => '41.850033'
				,	'lng_value'      => '-87.6500523'
				,	'zoom_value'     => '8'
				,	'zoom_wheel'     => 'no'
				,	'custom_class'  => ''
			), $atts));

			$random_id        = rand();
			$lat_value        = floatval( $lat_value );
			$lng_value        = floatval( $lng_value );
			$zoom_value       = intval( $zoom_value );
			$zoom_wheel       = $zoom_wheel=='yes' ? 'true' : 'false';

			$output = '<div class="google-map-api '.$custom_class.'"><span class="empty-block"></span>';
			$output .= '<div id="map-canvas-'.$random_id.'" class="gmap"></div>';
			$output .= '</div>';
			$output .= '<script type="text/javascript">
					google_api_map_init_'.$random_id.'();
					function google_api_map_init_'.$random_id.'(){
						var map;
						var coordData = new google.maps.LatLng(parseFloat('.$lat_value.'), parseFloat('.$lng_value.'));
						var marker;

						function initialize() {
							var mapOptions = {
								zoom: '.$zoom_value.',
								center: coordData,
								scrollwheel: '.$zoom_wheel.',
								mapTypeControl: false,
								streetViewControl: false,
								panControl: false,
								zoomControl: false,		
							}
							var map = new google.maps.Map(document.getElementById("map-canvas-'.$random_id.'"), mapOptions);
							var image = "'.CHILD_URL.'/images/marker.png";
							marker = new google.maps.Marker({
								map:map,
								draggable:true,
								position: coordData,
								icon: image
							});
						}
						google.maps.event.addDomListener(window, "load", initialize);
					}
					
			</script>';
			return $output;
		}
		add_shortcode('google_map_api', 'google_map_api_shortcode');
	}

	//------------------------------------------------------
	//  Related Posts
	//------------------------------------------------------
	if(!function_exists('cherry_related_posts')){
		function cherry_related_posts($args = array()){
			global $post;
			$default = array(
				'post_type' => get_post_type($post),
				'class' => 'related-posts',
				'class_list' => 'related-posts_list',
				'class_list_item' => 'related-posts_item',
				'display_title' => true,
				'display_link' => true,
				'display_thumbnail' => true,
				'width_thumbnail' => 170,
				'height_thumbnail' => 168,
				'before_title' => '<h3 class="related-posts_h">',
				'after_title' => '</h3>',
				'posts_count' => 4
			);
			extract(array_merge($default, $args));

			$post_tags = wp_get_post_terms($post->ID, $post_type.'_tag', array("fields" => "slugs"));
			$tags_type = $post_type=='post' ? 'tag' : $post_type.'_tag' ;
			$suppress_filters = get_option('suppress_filters');// WPML filter
			$blog_related = apply_filters( 'cherry_text_translate', of_get_option('blog_related'), 'blog_related' );
			if ($post_tags && !is_wp_error($post_tags)) {
				$args = array(
					"$tags_type" => implode(',', $post_tags),
					'post_status' => 'publish',
					'posts_per_page' => $posts_count,
					'ignore_sticky_posts' => 1,
					'post__not_in' => array($post->ID),
					'post_type' => $post_type,
					'suppress_filters' => $suppress_filters
					);
				query_posts($args);
				if ( have_posts() ) {
					$output = '<div class="'.$class.'">';
					$output .= $display_title ? $before_title.$blog_related.$after_title : '' ;
					$output .= '<ul class="'.$class_list.' clearfix">';
					while( have_posts() ) {
						the_post();
						$thumb   = has_post_thumbnail() ? get_post_thumbnail_id() : PARENT_URL.'/images/empty_thumb.gif';
						$blank_img = stripos($thumb, 'empty_thumb.gif');
						$img_url = $blank_img ? $thumb : wp_get_attachment_url( $thumb,'full');
						$image   = $blank_img ? $thumb : aq_resize($img_url, $width_thumbnail, $height_thumbnail, true) or $img_url;

						$output .= '<li class="'.$class_list_item.'">';
						$output .= $display_thumbnail ? '<figure class="thumbnail featured-thumbnail"><a href="'.get_permalink().'" title="'.get_the_title().'"><img data-src="'.$image.'" alt="'.get_the_title().'" /></a></figure>': '' ;
						$output .= $display_link ? '<a href="'.get_permalink().'" >'.get_the_title().'</a>': '' ;
						$output .= '</li>';
					}
					$output .= '</ul></div>';
					echo $output;
				}
				wp_reset_query();
			}
		}
	}

	/*-----------------------------------------------------------------------------------*/
	/* Custom Comments Structure
	/*-----------------------------------------------------------------------------------*/
	if ( !function_exists( 'mytheme_comment' ) ) {
		function mytheme_comment($comment, $args, $depth) {
			$GLOBALS['comment'] = $comment;
		?>
		<li <?php comment_class('clearfix'); ?> id="li-comment-<?php comment_ID() ?>">
			<div id="comment-<?php comment_ID(); ?>" class="comment-body clearfix">
				<div class="wrapper">
					<div class="comment-author vcard">
						<?php echo get_avatar( $comment->comment_author_email, 70 ); ?>
						<?php printf('<span class="author">%1$s</span>', get_comment_author_link()) ?>
					</div>
					<?php if ($comment->comment_approved == '0') : ?>
						<em><?php echo theme_locals("your_comment") ?></em>
					<?php endif; ?>
					<div class="extra-wrap">
						<?php echo esc_html( get_comment_text() ); ?>

						<div class="extra-wrap">
							<div class="reply">
								<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
							</div>
							<div class="comment-meta commentmetadata"><?php printf('%1$s', get_comment_date()) ?></div>
						</div>
					</div>
				</div>				
			</div>
	<?php }
	}
?>