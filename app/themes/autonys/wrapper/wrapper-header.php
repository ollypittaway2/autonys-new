<?php /* Wrapper Name: Header */ ?>
<div class="tail-top">
	<div class="extra-container">
		<div class="row">
			<div class="span4" data-motopress-type="static" data-motopress-static-file="static/static-logo.php">
				<?php get_template_part("static/static-logo"); ?>
			</div>	
			<div class="span8 pull-right">
				<div data-motopress-type="dynamic-sidebar" data-motopress-sidebar-id="header-sidebar">
					<?php dynamic_sidebar("header-sidebar"); ?>
				</div>	
				<div data-motopress-type="static" data-motopress-static-file="static/static-phone.php">
					<?php get_template_part("static/static-phone"); ?>
				</div>
			</div>				
		</div>
	</div>
</div>
<div class="nav-wrapper">
	<div class="extra-container">
		<div class="row">
			<div class="span9" data-motopress-type="static" data-motopress-static-file="static/static-nav.php">
				<?php get_template_part("static/static-nav"); ?>
			</div>
			<div class="span3 pull-right" data-motopress-type="static" data-motopress-static-file="static/static-search.php">
				<?php get_template_part("static/static-search"); ?>
			</div>
		</div>
	</div>
</div>