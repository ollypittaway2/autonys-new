<?php /* Static Name: Footer Logo */ ?>
<!-- BEGIN LOGO -->
<div class="footer-logo">
	<?php if(of_get_option('f_logo_url') == ''){ ?>
		<a href="<?php echo home_url(); ?>/" class="site-name"><?php bloginfo('name'); ?></a>
	<?php } else { ?>
		<a href="<?php echo home_url(); ?>/"><img src="<?php echo of_get_option('f_logo_url', '' ); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('description'); ?>"></a>
	<?php } ?>
	<p><?php echo of_get_option("f_desc"); ?></p>
</div>
<!-- END LOGO -->